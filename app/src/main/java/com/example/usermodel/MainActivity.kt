package com.example.usermodel

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    var personList = arrayListOf(
        lecturer,
        mentor,
        student1,
        student2,
        student3,
        student4,
        student5,
        student6,
        student7,
        student8,
        student9,
        student10,
        student11,
        student12,
        student13,
        student14,
        student15,
        student16,
        student17,
        student18,
        student19,
        student20,
        student21,
        student22,
        student23,
        student24,
        student25,
        student26
    )

    private fun init() {
//      index isn't 0 at first, because first person's text appears on textViews:
        var index = 1
        nextButton.setOnClickListener {
            getPerson(personList[index])
//            index growth is equal to button click count:
            index += 1
            //  in order to avoid crush:
            if (index == personList.size) {
                index = 0
            }
        }

        backButton.setOnClickListener {
            index -=1
            getPerson(personList[index])
            if (index == 0) {
                index = personList.size
            }
        }
    }

    private fun getPerson(userModel: UserModel) {
        nameTextView.text = userModel.name
        statusTextView.text = userModel.status
        emailTextView.text = userModel.Email
        var value = ""
        for (item in userModel.languages)
            value += "$item "
        languageTextView.text = value


    }
}






