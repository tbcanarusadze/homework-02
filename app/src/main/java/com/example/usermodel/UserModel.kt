package com.example.usermodel

class UserModel (var name: String,
                 var status: String,
                 var Email: String,
                 var languages: List<String>)

var lecturer = UserModel(
    "Nika Tabatadze",
    "Lecturer",
    "nikatabatadze9@gmail.com",
    arrayListOf("kotlin", "java", "swift")
)
var mentor = UserModel(
    "Sandro Kakhetelidze",
    "Mentor",
    "thesandro1998@gmail.com",
    arrayListOf("kotlin", "python")
)
var student1 = UserModel(
    "Ana Rusadze",
    "Student",
    "ana.rusadze.1@btu.edu.ge",
    arrayListOf("kotlin", "python")
)
var student2 = UserModel(
    "Lika Ghlonti",
    "Student",
    "lika.ghlonti.1@btu.edu.ge",
    arrayListOf("kotlin", "python")
)
var student3 = UserModel(
    "Ana Morchiladze",
    "Student",
    "ana.morchiladze.1@btu.edu.ge",
    arrayListOf("kotlin", "python")
)
var student4 = UserModel(
    "Beka Metreveli",
    "Student",
    "bekametreveliofficial@gmail.com",
    arrayListOf("kotlin", "python")
)
var student5 = UserModel(
    "Lika Diasamidze",
    "Student",
    "Likadiasa@gmail.com",
    arrayListOf("kotlin", "python")
)
var student6 = UserModel(
    "saba liklikadze",
    "Student",
    "saba.liklikadze1@gmail.com",
    arrayListOf("kotlin", "python")
)
var student7 = UserModel(
    "Marta Melia",
    "Student",
    "melia.marta@yahoo.com",
    arrayListOf("kotlin", "python")
)
var student8 = UserModel(
    "Giga Sulkhanishvili",
    "Student",
    "giga.sulkhanishvili.1@btu.edu.ge",
    arrayListOf("kotlin", "python")
)
var student9 = UserModel(
    "Grigor Koroyan",
    "Student",
    "mailkoroyan@gmail.com",
    arrayListOf("kotlin", "python")
)
var student10 = UserModel(
    "Davit Beriashvili",
    "Student",
    "dato.beriashvili@gmail.com",
    arrayListOf("kotlin", "python")
)
var student11 = UserModel(
    "გიორგი ნაცვლიშვილი",
    "Student",
    "giorgihnatsvlishvili@gmail.com",
    arrayListOf("kotlin", "python")
)
var student12 = UserModel(
    "Vazha Araviashvili",
    "Student",
    "17200143@ibsu.edu.ge",
    arrayListOf("kotlin", "python")
)
var student13 = UserModel(
    "Davit Soitashvili",
    "Student",
    "davit.soitashvili.1@btu.edu.ge",
    arrayListOf("kotlin", "python")
)
var student14 = UserModel(
    "Lasha Gogilava",
    "Student",
    "lashuna2001@gmail.com",
    arrayListOf("kotlin", "python")
)
var student15 = UserModel(
    "ირაკლი ჩხიტუნიძე",
    "Student",
    "i_chkhitunidze@cu.edu.ge",
    arrayListOf("kotlin", "python")
)
var student16 = UserModel(
    "Tedo Manvelidze",
    "Student",
    "tedex.manvelidze@gmail.com",
    arrayListOf("kotlin", "python")
)
var student17 = UserModel(
    "Shako Midelauri",
    "Student",
    "shakomidelauri@gmail.com",
    arrayListOf("kotlin", "python")
)
var student18 = UserModel(
    "Dimitri Altunashvili",
    "Student",
    "d.altunashvili@gmail.com",
    arrayListOf("kotlin", "python")
)
var student19 = UserModel(
    "Nika Rukhadze",
    "Student",
    "nikarukhadze.n@gmail.com",
    arrayListOf("kotlin", "python")
)
var student20 = UserModel(
    "Nika Dzigvashvili",
    "Student",
    "dzigvashvili@gmail.com",
    arrayListOf("kotlin", "python")
)
var student21 = UserModel(
    "Iona Bartishvili",
    "Student",
    "iona.bartishvili@gmail.com",
    arrayListOf("kotlin", "python")
)
var student22 = UserModel(
    "Tsotne Tsirekidze",
    "Student",
    "tsotne.tsirekidze.1@btu.edu.ge",
    arrayListOf("kotlin", "python")
)
var student23 = UserModel(
    "luka sheklashvili",
    "Student",
    "luka.sheylashvili@gmail.com",
    arrayListOf("kotlin", "python")
)
var student24 = UserModel(
    "nika chapidze",
    "Student",
    "nikachapidze01@gmail.com",
    arrayListOf("kotlin", "python")
)
var student25 = UserModel(
    "Lasha Kakulia",
    "Student",
    "momakvdavipantera@gmail.com",
    arrayListOf("kotlin", "python")
)
var student26 = UserModel(
    "luka parchukidze ",
    "Student",
    "luka.parchukidze.1@btu.edu.ge",
    arrayListOf("kotlin", "python")
)
